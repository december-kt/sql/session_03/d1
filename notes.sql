-- To insert data into the table
-- Syntax: INSERT INTO table_name (col_name) VALUES (data_value);
-- Examples:
INSERT INTO artists (name) VALUES ("Blackpink");
INSERT INTO artists (name) VALUES ("Twice");
INSERT INTO artists (name) VALUES ("Taylor Swift");

-- To display a spefic column from a table
-- Syntax: SELECT col_name FROM table_name;
-- Example:
SELECT name FROM artists;

-- To display all columns from a table
-- In SQL, asterisk (*) means all
-- Syntax: SELECT * FROM table_name;
-- Examples:
SELECT * FROM artists;
SELECT * FROM songs;

-- To add multiple data into the table
-- Syntax: INSERT INTO table_name (col_name_1, col_name_2, ... ,col_name_n) VALUES (data_1, data_2, ... , data_n);
-- Examples:
INSERT INTO albums (album_title, date_released, artist_id) VALUES ("The Album", "2020-10-02", 1);
INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Fancy You", "2019-10-02", 2);
INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Red (Taylor's Version)", "2021-10-02", 3);

-- You can also add multiple data with just one line command;
-- Example:
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Fancy", 333, "K-Pop", 2), ("How You Like That", 301, "K-Pop", 1), ("All Too Well (10 mins version)", 1000, "Pop", 3);

-- To display specific columns in a table
-- Syntax: SELECT col_name_1, col_name_2 FROM table_name;
-- Exmaple:
SELECT song_name, genre FROM songs;

-- To display specific columns with conditions in a table
-- WHERE is used for conditions
-- Syntax: SELECT col_name_1, col_name_2 FROM table_name WHERE condition;
-- Examples:
SELECT song_name FROM songs WHERE genre = "K-Pop";
SELECT song_name FROM songs WHERE length >= 1000;

-- To display specific column with multiple condition in a table
-- AND/OR is used for multiple conditions
-- Syntax: SELECT col_name_1, col_name_2 FROM table_name WHERE condition_1 AND condition_2;
-- Syntax: SELECT col_name_1, col_name_2 FROM table_name WHERE condition_1 OR condition_2;
-- Examples:
SELECT song_name FROM songs where genre = "Pop" AND length >= 1000;
SELECT song_name FROM songs where genre = "Pop" OR length >= 1000;

-- To update the data in a column
-- Syntax: UPDATE table_name SET col_name = "NEW VALUE" WHERE col_name = "EXISTING VALUE/UNIQUE IDENTIFIER";
-- Examples:
UPDATE songs SET song_name = "Feel Special" WHERE song_name = "Paraluman";
UPDATE songs SET song_name = "Babalik Sayo" WHERE song_name = "Feel Special" AND genre = "Pop";

-- To delete a data from the column
-- Syntax: DELETE FROM table_name WHERE condition_1;
-- Examples:
DELETE FROM songs WHERE genre = "OPM" and length = "410";

-- Each datatype of sql will have its own format. You can search fro it if you would like.